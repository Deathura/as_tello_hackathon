import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Joy
from std_msgs.msg import String, Empty
from geometry_msgs.msg._twist import Twist
import cv2

class Controller(Node):

    def __init__(self):
        super().__init__('controller')
        self.publisher_land = self.create_publisher(Empty, '/land', 1)
        self.publisher_flip = self.create_publisher(String, '/flip', 1)
        self.publisher_takeoff = self.create_publisher(Empty, '/takeoff', 1)
        self.publisher_secure_cmd = self.create_publisher(Twist, '/control', 1)
        self.publisher_emergency = self.create_publisher(Empty, '/emergency', 1)
        self.subscription = self.create_subscription(Joy, '/joy', self.joy_callback, 10)
        self.subscription = self.create_subscription(Empty, '/takingoff', lambda msg: self.set_bool_state(True), 10)
        self.subscription = self.create_subscription(Empty, '/land', lambda msg: self.set_bool_state(False), 10)
        
        self.last_message = None
        self.bool = False
    
    def set_bool_state(self, state: bool):
        self.bool = state

    def manual_control(self, mannette):
        speed = 60
        msg = Twist()

        #############################################
        # start editable part
        #############################################
        
        msg.angular.z = -mannette.axes[3] * speed
        msg.linear.x = mannette.axes[0] * speed
        msg.linear.y = mannette.axes[1] * speed
        msg.linear.z = mannette.axes[4] * speed

        #############################################
        # /!\ end editable part
        #############################################

        self.publisher_secure_cmd.publish(msg)



    def joy_callback(self, msg):

        if(msg.buttons[3] == 1 and self.last_message.buttons[3] != 1 and self.bool == True):
            self.publisher_land.publish(Empty())
            self.bool=False
            print("LANDING")

        if(msg.buttons[0] == 1 and self.last_message.buttons[0] != 1 and self.bool == False):
            self.publisher_takeoff.publish(Empty())
            self.bool=True
            print("TAKINGOFF")
            
        if(msg.buttons[1] == 1 and self.last_message.buttons[1] != 1):
            self.publisher_emergency.publish(Empty())
            print("RESET")

        if(self.last_message == None or msg.axes[0] != self.last_message.axes[0] or msg.axes[1] != self.last_message.axes[1] or msg.axes[4] != self.last_message.axes[4] or msg.axes[3] != self.last_message.axes[3]):
            self.manual_control(msg)
        
        #############################################
        # start editable part
        #############################################

       
       # if():
       #     message = String()
       #     message.data = 'r'
       # 
       #     # find the command to send the backflip order
       #  
       #     print("FLIP")

        #############################################
        # /!\ end editable part
        #############################################

        self.last_message = msg

def main(args=None):
    rclpy.init(args=args)
    controller = Controller()
    rclpy.spin(controller)
    controller.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
